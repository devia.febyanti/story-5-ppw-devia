from django.db import models

# Create your models here.
class Post(models.Model):
    nama_matkul = models.CharField(max_length=50, verbose_name="Mata Kuliah", primary_key=True, default=None)
    dosen = models.CharField(max_length=30, default=None)
    jumlah_sks = models.IntegerField(verbose_name="Jumlah SKS", default=None)
    deskripsi_matkul = models.TextField(blank=True, default=None)
    semester_tahun = models.CharField(max_length=20, default=None)
    ruang_kelas = models.CharField(max_length=15, verbose_name="Ruang Kelas", default=None)