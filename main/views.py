from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from .models import Post
from .forms import tambahMatkul, hapusMatkul

def home(request):
    return render(request, 'main/home.html')

def hobi(request):
    return render(request, 'main/hobi.html')

def skill(request):
    return render(request, 'main/skill.html')

def pendidikan(request):
    return render(request, 'main/pendidikan.html')

class mataKuliah(TemplateView):
    namaTemplate = "main/tambah_matkul.html"
    def get(self, req):
        daftarMatkul = [(nama_matkul.nama_matkul, nama_matkul.nama_matkul) for nama_matkul in Post.objects.all()]
        form = tambahMatkul()
        form_delete = hapusMatkul(daftarMatkul)
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})
    
    def post(self, req):
        form = tambahMatkul(req.POST)
        if form.is_valid():
            form.save()
            return redirect("main:mata-kuliah")
        return render(req, self.namaTemplate, {"form": form})

class hapusMataKuliah(TemplateView):
    namaTemplate = "main/tambah_matkul.html"
    def get(self, req):
        daftarMatkul = [(nama_matkul.nama_matkul, nama_matkul.nama_matkul) for nama_matkul in Post.objects.all()]
        form = tambahMatkul()
        form_delete = hapusMatkul(daftarMatkul)
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})
    
    def post(self,req):
        form = tambahMatkul()
        daftarMatkul = [(nama_matkul.nama_matkul, nama_matkul.nama_matkul) for nama_matkul in Post.objects.all()]
        form_delete = hapusMatkul(daftarMatkul, req.POST)
        if form_delete.is_valid():
            print(form_delete)
            Post.objects.get(nama_matkul=req.POST.get("mataKuliah")).delete()
            return redirect("main:mata-kuliah")
        return render(req, self.namaTemplate, {"form": form, "delete": form_delete})
    
def get_matkul(req):
    mataKuliah = Post.objects.all()
    return render(req, "main/jadwal.html", {'mataKuliah': mataKuliah})

def get_daftar_matkul(req, name):
    nama_matkul = Post.objects.get(pk=name)
    return render(req, "main/matkul.html", {"nama_matkul": nama_matkul})
