from django import forms
from .models import Post
from crispy_forms.helper import FormHelper

class tambahMatkul(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'

class hapusMatkul(forms.Form):
    def __init__(self, mataKuliah, *args, **kwargs):
        super(hapusMatkul, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields["mataKuliah"].choices = mataKuliah
    
    mataKuliah = forms.ChoiceField(choices=(), required=True, label="Mata Kuliah")