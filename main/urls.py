from django.urls import include, path
from .views import home, hobi, skill, pendidikan, mataKuliah, get_matkul, hapusMataKuliah, get_daftar_matkul

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('home', home, name='home'),
    path('hobi', hobi, name='hobi'),
    path('skill', skill, name='skill'),
    path('pendidikan', pendidikan, name='pendidikan'),
    path('tambah-mata-kuliah', mataKuliah.as_view(), name='tambah-mata-kuliah'),
    path('hapus-mata-kuliah', hapusMataKuliah.as_view(), name='hapus-mata-kuliah'),
    path('jadwal', get_matkul, name='mata-kuliah'),
    path('matkul/<str:name>', get_daftar_matkul, name='daftar-mata-kuliah')
]
